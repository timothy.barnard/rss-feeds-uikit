//
//  FeedViewModel.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal struct FeedViewModel {
    
    internal var imageLoader: ((UIImage?) -> Void)?
    internal var name: String?
    internal var dateStr: String?
    internal var link: URL?
    internal var canDelete: Bool = false
    internal var feedEntry: FeedEntry
    
    private var imageURL: URL?
    
    internal var imageService: ImageService? {
        didSet {
            self.loadImage()
        }
    }
    
    init(_ feedEntry: FeedEntry) {
        self.feedEntry = feedEntry
        self.link = feedEntry.url
        self.imageURL = feedEntry.imageURL
        self.name = feedEntry.name
        self.dateStr = feedEntry.publishDate.toString
    }
    
    mutating func loadImage() {
        if let imageURl = imageURL {
            let closureSelf = self
            self.imageService?.loadImage(from: imageURl, retrievedImage: { (result: Result<UIImage?, Error>) in
                switch result {
                case .success(let image):
                    closureSelf.imageLoader?(image)
                case .failure(let error):
                    print(error)
                }
            })
        }
    }
    
}
