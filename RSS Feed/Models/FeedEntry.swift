//
//  FeedEntry.swift
//  RSS Feeds
//
//  Created by Timothy on 19/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

struct RSSFeed: Codable {
    var name: String?
    var feedURL: String?
    var feedTag: String?
    var slug: String?
    
    enum CodingKeys: String, CodingKey {
        case name, slug
        case feedURL = "feed_url"
        case feedTag = "feed_tag"
    }
}

struct FeedEntry: Codable {
    
    var id: Int?
    var name: String?
    var link: String?
    var summary: String?
    var publishDate: Date?
    var mediaURL: String?
    var tags: [String]?
    var rssFeed: RSSFeed?
    
    enum CodingKeys: String, CodingKey {
        case name, link, summary, tags, id
        case publishDate = "publish_date"
        case mediaURL = "media_url"
        case rssFeed = "rss_feed"
    }
    
    static func == (lhs: FeedEntry, rhs: FeedEntry) -> Bool {
        return lhs.id == rhs.id
    }
    
    var request: URLRequest {
        let url = URL(string: link!)!
        let urlRequest = URLRequest(url: url)
        return urlRequest
    }
    
    var url: URL? {
        guard let value = self.link else {return nil}
        return URL(string: value)
    }
    
    var imageURL: URL? {
        guard let value = self.mediaURL else {return nil}
        return URL(string: value)
    }
}

struct FeedEntryResponse: Codable {
    var count: Int?
    var next: String?
    var previous: String?
    var results: [FeedEntry]?
    
    enum CodingKeys: String, CodingKey {
        case count, next, previous, results
    }
}
