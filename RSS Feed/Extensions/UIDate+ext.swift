//
//  UIDate+ext.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

extension Optional where Wrapped == Date {
    
    var toString: String {
        guard let date = self else {return "--"}
        let dateFormmater = DateFormatter()
        dateFormmater.dateStyle = .short
        dateFormmater.timeStyle = .short
        return dateFormmater.string(from: date)
    }
    
}
