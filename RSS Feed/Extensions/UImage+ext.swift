//
//  UImage+ext.swift
//  RSS Feed
//
//  Created by Timothy on 02/07/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

extension UIImage {
    func isEqual(to image: UIImage) -> Bool {
        if let lhsData = self.pngData(), let rhsData = image.pngData() {
            return lhsData == rhsData
        }
        return false
    }
}
