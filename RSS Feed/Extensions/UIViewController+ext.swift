//
//  UIViewController+ext.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UIViewController {
    
    /// SHow alert view controller
    ///
    /// - Parameters:
    ///   - title: Alert title
    ///   - message: Alert message
    func showAlert(_ title: String?, message: String?) {
        let alertController = UIAlertController(
            title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
