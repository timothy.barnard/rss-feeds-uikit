//
//  DataLoader.swift
//  RSS Feed
//
//  Created by Timothy on 02/07/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

protocol DataLoader {
    var canDelete: Bool { get }
    var canSave: Bool { get }
    func saveItem( _ feedEntry: FeedEntry)
    @discardableResult func deleteItem(_ feedEntry: FeedEntry) -> Bool
    func getAllItems(_ endPoint: Endpoints?, retrieved: @escaping ( Result<[FeedEntry]?, Error>) -> Void)
}
