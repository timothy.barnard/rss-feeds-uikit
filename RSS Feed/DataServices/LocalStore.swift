//
//  LocalStore.swift
//  RSS Feed
//
//  Created by Timothy on 02/07/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class LocalStore: DataLoader {
    
    var canDelete: Bool { return true }
    var canSave: Bool { return false }
    
    var feedEntryLocalStore: FeedEntryLocalStore?
    
    init() {
        self.feedEntryLocalStore = FeedEntryLocalStore()
    }
    
    func getAllItems(_ endPoint: Endpoints?, retrieved: @escaping (Result<[FeedEntry]?, Error>) -> Void) {
        let items = self.feedEntryLocalStore?.getFavourites()
        retrieved(.success(items))
    }
    
    @discardableResult
    func deleteItem(_ feedEntry: FeedEntry) -> Bool {
        return self.feedEntryLocalStore?.deleteFeedEntry(feedEntry) ?? false
    }
    
    func saveItem(_ feedEntry: FeedEntry) {}
}
