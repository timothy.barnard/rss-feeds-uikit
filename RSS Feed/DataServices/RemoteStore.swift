//
//  RemoteStore.swift
//  RSS Feed
//
//  Created by Timothy on 02/07/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class RemoteStore: DataLoader {
    
    var canSave: Bool { return true }
    var canDelete: Bool { return false }
    
    var feedEntryLocalStore: FeedEntryLocalStore?
    var networkService: NetworkingService?
    
    init() {
        self.networkService = NetworkingService()
        self.feedEntryLocalStore = FeedEntryLocalStore()
    }
    
    @discardableResult
    func deleteItem(_ feedEntry: FeedEntry) -> Bool {
        return false
    }
    
    func saveItem(_ feedEntry: FeedEntry) {
        self.feedEntryLocalStore?.saveFeedEntry(feedEntry)
    }
    
    func getAllItems(_ endPoint: Endpoints?, retrieved: @escaping (Result<[FeedEntry]?, Error>) -> Void) {
        networkService?.makeRequest(endPoint!) { (result: Result<FeedEntryResponse?, NetworkingService.NetworkError>) in
            switch result {
            case .success(let feedEntryResponse):
                retrieved(.success(feedEntryResponse?.results))
            case .failure(let error):
                print(error)
                retrieved(.failure(error))
            }
        }
    }
}
