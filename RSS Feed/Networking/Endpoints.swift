//
//  Endpoints.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

/// Enum for list of available endpoints
enum Endpoints {
    
    enum Search {
        case all
        case tech
        case code
        case search(String)
        
        var code: String {
            switch self {
            case .all: return "All"
            case .tech: return "Tech"
            case .code: return "Code"
            case .search: return "Search"
            }
        }
        
        static var list: [Search] {return [.all, .code, .tech]}
    }
    
    case feeds(Search)
    case today(Search)
    
    func find(_ search: Search) -> Endpoints {
        switch self {
        case .feeds: return Endpoints.feeds(search)
        case .today: return Endpoints.today(search)
        }
    }
}

extension Endpoints {
    
    /// returns the base URL
    var baseURL: URL {
        guard let url = URL(string: "https://\(self.host)") else {
            fatalError("Missing base url")
        }
        return url
    }
    
    /// host
    var host: String {
        guard let value = Config.apiURL.value else {
            fatalError("Missing base url")
        }
        return value
    }
    
    /// return api token
    var token: String {
        guard let token = Config.apiToken.value else {
            fatalError("Missing token")
        }
        return token
    }
    
    /// returns path for each item
    var path: String {
        switch self {
        case .feeds(let search): return "/api/v2/feeds/"
        case .today(let search): return "/api/v2/feeds/today/"
        }
    }
    
    var queryItems: [URLQueryItem] {
        return [URLQueryItem(name: "access_key", value: self.token)]
    }
    
    /// Complete URL for each endpoint
    var url: URL? {
        var urlComponents = URLComponents()
        urlComponents.host = self.host
        urlComponents.scheme = "https"
        urlComponents.path = self.path
        urlComponents.queryItems = self.queryItems
        return urlComponents.url
    }
}
