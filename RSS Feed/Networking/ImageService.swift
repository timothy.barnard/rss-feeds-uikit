//
//  ImageService.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal final class ImageService {
    
    var cacheImages = NSCache<NSString, UIImage>()
    
    internal func loadImage(from url: URL, retrievedImage: @escaping (Result<UIImage?, Error>) -> Void) {
        if let cachedImage = cacheImages.object(forKey: url.path as NSString) {
            DispatchQueue.main.async {
                retrievedImage(.success(cachedImage))
            }
        } else {
            URLSession.shared.dataTask(with: url) { data, respsonse, error in
                guard let data = data, let mimeType = respsonse?.mimeType,  mimeType.hasPrefix("image"), let image = UIImage(data: data) else {
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    DispatchQueue.main.async {
                        retrievedImage(.failure(error!))
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.cacheImages.setObject(image, forKey: url.path as NSString , cost: 1)
                    retrievedImage(.success(image))
                }
            }.resume()
        }
    }

    
}
