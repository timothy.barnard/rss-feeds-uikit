//
//  NetworkingService.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

/// Networking class for all requests
final class NetworkingService {
    
    enum NetworkError: Error {
        case invalidURL
        case network(Error?)
        case decoding(Error?)
    }
    
    enum DateFormats: String, CaseIterable {
        case withTimezone = "yyyy-MM-dd'T'HH:mm:ssZ"
        case withoutTimezone = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    }
    
    private var session: URLSession!
    private var task: URLSessionTask?
    
    private var dateFormmater: DateFormatter {
        let dateFormater = DateFormatter()
        dateFormater.calendar = Calendar.current
        dateFormater.locale = Locale(identifier: "en_GB")
        dateFormater.timeZone = TimeZone(abbreviation: "UTC")
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormater
    }
    
    init(_ session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    
    /// Making a network request
    ///
    /// - Parameters:
    ///   - endpoint: Endpoints - the endpoint which to make the request
    ///   - completed: callback with Rsult type with Optioanl T and Error
    internal func makeRequest<T: Codable>(_ endpoint: Endpoints, completed: @escaping( Result<T?, NetworkingService.NetworkError>) -> Void ) {
        
        guard let url = endpoint.url else {
            completed(.failure(NetworkError.invalidURL))
            return
        }
        
        self.task = self.session.dataTask(with: url, completionHandler: { (data, response, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completed(.failure(NetworkingService.NetworkError.network(error)))
                }
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .custom { decoder in
                    let container = try decoder.singleValueContainer()
                    let dateString = try container.decode(String.self)
                    
                    let dateFormmater = self.dateFormmater
                    for date in DateFormats.allCases {
                        dateFormmater.dateFormat = date.rawValue
                        if let date = dateFormmater.date(from: dateString) {
                            return date
                        }
                    }
                    
                    throw DecodingError.dataCorruptedError(in: container, debugDescription: "Unable to decode date: \(dateString)")
                }
                let object: T? = try jsonDecoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    completed(.success(object))
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    completed(.failure(NetworkError.decoding(error)))
                }
            }
        })
        self.task?.resume()
    }
    
    /// Cancels the current tasks
    internal func cancelRequest() {
        self.task?.cancel()
    }
    
}
