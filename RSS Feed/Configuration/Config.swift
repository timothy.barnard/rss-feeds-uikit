//
//  Config.swift
//  RSS Feeds
//
//  Created by Timothy on 20/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

enum Config: String {
    case apiURL = "API_URL"
    case apiToken = "API_TOKEN"
    
    var value: String? {
        return self.getKeyForValue(self)
    }
    
    private func getKeyForValue(_ key: Config) -> String? {
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            return dict[key.rawValue] as? String
        }
        return nil
    }
}
