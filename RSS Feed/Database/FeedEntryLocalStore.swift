//
//  FeedEntryLocalStore.swift
//  RSS Feed
//
//  Created by Timothy on 27/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import CoreData

internal final class FeedEntryLocalStore {
    
    internal var dataChange: (() -> Void)?
    
    private func convertFeedEntry(_ feedsEntryDBs: [FeedEntryDB]?) -> [FeedEntry]? {
        guard let entries = feedsEntryDBs else { return nil }
        return entries.map({ (entry) -> FeedEntry in
            return FeedEntry(
                id: Int(entry.id),
                name: entry.name,
                link: entry.name,
                summary: entry.summary,
                publishDate: entry.publishDate as Date?,
                mediaURL: entry.imagePath,
                tags: nil,
                rssFeed: nil
            )
        })
    }
    
    internal func getFavourites() -> [FeedEntry]? {
        let context = CoreDataManager.shared.context
        let fetchRequest = FeedEntryDB.fetchRequest() as NSFetchRequest<FeedEntryDB>
        do {
            let entries = try context.fetch(fetchRequest)
            return self.convertFeedEntry(entries)
        } catch {
            print("Retrieving error: \(error)")
            return nil
        }
    }
    
    internal func saveFeedEntry(_ feedEntry: FeedEntry) {
        guard let id = feedEntry.id  else { return}
        let context = CoreDataManager.shared.context
        let feedEntryDB = FeedEntryDB(context: context)
        feedEntryDB.id = Int32(id)
        feedEntryDB.name = feedEntry.name
        feedEntryDB.link = feedEntry.link
        feedEntryDB.summary = feedEntry.summary
        feedEntryDB.imagePath = feedEntry.imageURL?.absoluteString ?? nil
        feedEntryDB.publishDate = feedEntry.publishDate as NSDate?
        CoreDataManager.shared.saveContext()
        self.dataChange?()
    }
    
    internal func deleteFeedEntry(_ feedEntry: FeedEntry) -> Bool {
        guard let id = feedEntry.id else {return false}
        let context = CoreDataManager.shared.context
        
        let fetchRequest = FeedEntryDB.fetchRequest() as NSFetchRequest<FeedEntryDB>
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        
        do {
            let foundEntry = try context.fetch(fetchRequest).first
            if let entry = foundEntry {
                context.delete(entry)
                self.dataChange?()
                return true
            } else {
                return false
            }
        } catch {
            print(error)
            return false
        }
    }
}
