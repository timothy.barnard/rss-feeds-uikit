//
//  FeedCollectionViewCell.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

class FeedCollectionViewCell: UICollectionViewCell {
    
    enum BookmarkState {
        case selected
        case unselected
        
        var image: UIImage {
            switch self {
            case .selected: return UIImage(named: "bookmark")!
            case .unselected: return UIImage(named: "bookmark_open")!
            }
        }
        
        var selectedState: Bool {
            return self == BookmarkState.selected
        }
        
        static func stateForImage(_ image: UIImage) -> BookmarkState {
            return image.isEqual(to: UIImage(named: "bookmark")!) ? .selected : .unselected
        }
        
        var flipState: BookmarkState {
            switch self {
            case .selected: return .unselected
            case .unselected: return .selected
            }
        }
    }
    
    let shadowView: UIView = {
        let label = UIView(frame: .zero)
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var feedImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = UIView.ContentMode.scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var bookmarkImageView: UIButton = {
        let button = UIButton(frame: .zero)
        button.setImage(UIImage(named: "bookmark_open"), for: .normal)
        button.setTitle(nil, for: .normal)
        button.tintColor = .blue
        button.addTarget(self, action: #selector(bookmarkPressed(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var feedNameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = UIColor.black
        label.textAlignment = .left
        label.numberOfLines = -1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var feedDateLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.darkGray
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    static let identifier = String(describing: FeedCollectionViewCell.self)
    
    internal var bookmarkPressed: ((Bool) -> Void)?
    
    internal var feedViewModel: FeedViewModel? {
        didSet {
            feedViewModel?.imageLoader = {
                self.feedImageView.image = $0
            }
            self.feedNameLabel.text = feedViewModel?.name
            self.feedDateLabel.text = feedViewModel?.dateStr
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        shadowView.addShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.backgroundColor = .white
        self.addSubview(shadowView)
        self.pinToEdges(shadowView, with: (0, 10, 5, 5))
        
        let verticalStackView = UIStackView(frame: .zero)
        verticalStackView.axis = NSLayoutConstraint.Axis.vertical
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.distribution = .fill
        verticalStackView.spacing = 10
        verticalStackView.alignment = .center
        shadowView.addSubview(verticalStackView)
        shadowView.pinToEdges(verticalStackView)
        
        self.feedNameLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.feedDateLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.feedImageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        
        verticalStackView.addArrangedSubview(feedImageView)
        verticalStackView.addArrangedSubview(feedDateLabel)
        verticalStackView.addArrangedSubview(feedNameLabel)
        
        self.addSubview(bookmarkImageView)
        
        NSLayoutConstraint.activate([
            feedNameLabel.leadingAnchor.constraint(equalTo: verticalStackView.leadingAnchor, constant: 5),
            feedNameLabel.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor, constant: -5),
            
            feedDateLabel.leadingAnchor.constraint(equalTo: verticalStackView.leadingAnchor, constant: 5),
            feedDateLabel.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor, constant: -5),
            
            bookmarkImageView.widthAnchor.constraint(equalToConstant: 30),
            bookmarkImageView.heightAnchor.constraint(equalToConstant: 40),
            bookmarkImageView.topAnchor.constraint(equalTo: verticalStackView.topAnchor, constant: -10),
            bookmarkImageView.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor, constant: 10)
            ])
    }
    
    @objc func bookmarkPressed(_ sender: UIButton) {
        let imageState: BookmarkState = BookmarkState.stateForImage(sender.currentImage!)
        sender.setImage(imageState.flipState.image, for: .normal)
        self.bookmarkPressed?(imageState.selectedState)
    }
}
