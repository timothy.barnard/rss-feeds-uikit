//
//  ViewController.swift
//  RSS Feed
//
//  Created by Timothy on 26/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit
import SafariServices

final class AllFeedsViewController: UIViewController {
    
    internal var endpoints: Endpoints?
    internal var dataLoader: DataLoader?
    
    // MARK: View Properties
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .white
        collectionView.register(FeedCollectionViewCell.self, forCellWithReuseIdentifier: FeedCollectionViewCell.identifier)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    // MARK: Local Properties
    private var feedViewModels = [FeedViewModel]()
    private var imageService = ImageService()
    
    override func loadView() {
        super.loadView()
        self.view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            self.view.topAnchor.constraint(equalTo: collectionView.topAnchor),
            self.view.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor),
            self.view.leadingAnchor.constraint(equalTo: collectionView.leadingAnchor),
            self.view.trailingAnchor.constraint(equalTo: collectionView.trailingAnchor),
            ])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
    }
    
    public func fetchData() {
        dataLoader?.getAllItems(self.endpoints, retrieved: { (result: Result<[FeedEntry]?, Error>) in
            switch result {
            case .success(let feedEntries):
                self.feedViewModels = feedEntries?.compactMap{ $0 }.map( { FeedViewModel($0) } ) ?? []
                self.collectionView.reloadData()
            case .failure(let error):
                self.showAlert("Error", message: error.localizedDescription)
            }
        })
    }
    
    private func shwoWebPage(with url: URL) {
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        let viewController = SFSafariViewController(url: url, configuration: config)
        self.present(viewController, animated: true, completion: nil)
    }
}

// MARK: UICollectionViewDelegate
extension AllFeedsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let feedModelView = self.feedViewModels[indexPath.row]
        guard let url = feedModelView.link else {
            self.showAlert("Error", message: "Retrieving the link")
            return
        }
        self.shwoWebPage(with: url)
    }
}

// MARK: UICollectionViewDataSource
extension AllFeedsViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.feedViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedCollectionViewCell.identifier, for: indexPath) as? FeedCollectionViewCell else {
            fatalError("FeedCollectionViewCell dequeue cell error")
        }
        cell.feedViewModel = self.feedViewModels[indexPath.row]
        cell.feedViewModel?.imageService = self.imageService
        cell.bookmarkPressed = { state in
            if state {
                self.dataLoader?.deleteItem(self.feedViewModels[indexPath.row].feedEntry)
            } else {
                self.dataLoader?.saveItem(self.feedViewModels[indexPath.row].feedEntry)
            }
        }
        return cell
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension AllFeedsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width - 20, height: self.view.frame.width * 0.8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.5
    }
}
