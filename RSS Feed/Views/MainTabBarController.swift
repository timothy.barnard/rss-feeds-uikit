//
//  MainNavigationController.swift
//  RSS Feed
//
//  Created by Timothy on 27/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressedBarItem(_:)))
        self.tabBar.addGestureRecognizer(longPressGesture)
        
        let allFeedsViewController = AllFeedsViewController()
        allFeedsViewController.endpoints = Endpoints.feeds(.all)
        allFeedsViewController.dataLoader = RemoteStore()
        allFeedsViewController.title = "All"
        allFeedsViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 0)
        
        let todayFeedsViewController = AllFeedsViewController()
        todayFeedsViewController.endpoints = Endpoints.today(.all)
        todayFeedsViewController.dataLoader = RemoteStore()
        todayFeedsViewController.title = "Today"
        todayFeedsViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .recents, tag: 1)
        
        let savedViewController = AllFeedsViewController()
        savedViewController.endpoints = Endpoints.today(.all)
        savedViewController.dataLoader = LocalStore()
        savedViewController.title = "Saved"
        savedViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 2)
        
        self.viewControllers = [todayFeedsViewController, allFeedsViewController, savedViewController]
    }
    
    @objc func longPressedBarItem(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for item in Endpoints.Search.list {
                actionSheetController.addAction(UIAlertAction(title: item.code, style: .default) { [weak self] action in
                    guard let currentEndpoint = (self?.selectedViewController as? AllFeedsViewController)?.endpoints else {
                        return
                    }
                    (self?.selectedViewController as? AllFeedsViewController)?.endpoints = currentEndpoint.find(item)
                    (self?.selectedViewController as? AllFeedsViewController)?.fetchData()
                })
            }
            actionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(actionSheetController, animated: true)
        }
    }
}

extension MainTabBarController {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == self.selectedIndex {
            (self.selectedViewController as? AllFeedsViewController)?.fetchData()
        }
    }
}
