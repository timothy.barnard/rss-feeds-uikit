//
//  FeedEntryDB+CoreDataProperties.swift
//  RSS Feed
//
//  Created by Timothy on 27/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//
//

import Foundation
import CoreData


extension FeedEntryDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FeedEntryDB> {
        return NSFetchRequest<FeedEntryDB>(entityName: "FeedEntryDB")
    }

    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var link: String?
    @NSManaged public var imagePath: String?
    @NSManaged public var summary: String?
    @NSManaged public var publishDate: NSDate?

}
